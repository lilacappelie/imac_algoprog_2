#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value=value;
        this->left=nullptr;
        this->right=nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if(value>this->value){
            if(this->right == nullptr){
                this->right = new SearchTreeNode(value);
            }
            else{
                this->right->insertNumber(value);
            }
        }
        if(value<this->value){
            if(this->left == nullptr){
                this->left = new SearchTreeNode(value);
            }
            else{
                this->left->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int hleft = 0;
        int hright = 0;
        if(isLeaf()){
            return 1;
        }
        if(this->right!=nullptr){
            hright = this->right->height();
        }
        if(this->left!=nullptr){
            hleft = this->left->height();
        }
        if(hright>hleft){
            return hright+1;
        }
        return hleft+1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int count = 0;
        if(isLeaf()){
            return 1;
        }
        if(left!=nullptr){
            count += left->nodesCount();
        }
        if(right!=nullptr){
            count += right->nodesCount();
        }
        return count + 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left==nullptr && this->right==nullptr){
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(isLeaf()){
            leaves[leavesCount] = this;
            leavesCount++;
        }
        else{
            if(this->left!=nullptr){
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right!=nullptr){
                this->right->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(left!=nullptr) left->inorderTravel(nodes, nodesCount);
        nodes[nodesCount] = this;
        nodesCount++;
        if(right!=nullptr) right->inorderTravel(nodes, nodesCount);
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        if(left!=nullptr) left->preorderTravel(nodes, nodesCount);
        if(right!=nullptr) right->preorderTravel(nodes, nodesCount);
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(left!=nullptr) left->postorderTravel(nodes, nodesCount);
        if(right!=nullptr) right->postorderTravel(nodes, nodesCount);
        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(this->value == value) return this;
        if(left!=nullptr) return left->find(value);
        if(right!=nullptr) return right->find(value);
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}

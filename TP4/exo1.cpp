#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex)
{   //retourne l’index du fils gauche du noeud à l’indice nodeIndex
    if(nodeIndex*2+1 < this->size()){
        return (nodeIndex*2)+1;
    }
    return 0;
}

int Heap::rightChildIndex(int nodeIndex)
{
    if(nodeIndex*2+1 < this->size()){
        return (nodeIndex*2)+2;
    }
    return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{   //insère un nouveau noeud dans le tas heap tout en gardant la propriété de tas
    // use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i) = value;
    while(i>0 && this->get(i)>this->get((i-1)/2)){
        this->swap(i,(i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{   //Si le noeud à l’indice nodeIndex n’est pas supérieur à ses enfants, reconstruit le tas à partir de cette index
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int left = leftChildIndex(nodeIndex);
    int right = rightChildIndex(nodeIndex);
    if(this->get(left)>this->get(nodeIndex) && this->get(left)>this->get(right)){
        i_max = left;
    }
    if(this->get(right)>this->get(nodeIndex) && this->get(right)>this->get(left)){
        i_max = right;
    }
    if(i_max != nodeIndex){
        this->swap(nodeIndex, i_max);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{   //Construit un tas à partir des valeurs de numbers (vous pouvez utiliser soit insertHeapNode soit heapify)
    for(int i = 0; i < numbers.size(); i++){
        insertHeapNode(numbers.size(),numbers[i]);
    }
}

void Heap::heapSort()
{   //Construit un tableau trié à partir d’un tas heap
    for(int i =this->size()-1; i>-1; i--){
        swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}

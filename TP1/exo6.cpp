#include <iostream>

using namespace std;

struct Noeud
{
    int donnee;
    Noeud *suivant;
};

struct Liste
{
    Noeud *premier;
    // your code
};

struct DynaTableau
{
    int capacite;
    int taille;
    int *tab;
};

void initialise(Liste *liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste *liste)
{
    if(liste->premier==nullptr){
        return true;
    }
    return false;
}

void ajoute(Liste *liste, int valeur)
{
    Noeud *nv = new Noeud;
    nv->donnee = valeur;
    nv->suivant = liste->premier;
    liste->premier = nv;
}

void affiche(const Liste *liste)
{
    Noeud* iterateur = liste->premier;
    while(iterateur!=nullptr){
        cout << iterateur->donnee << endl;
        iterateur = iterateur->suivant;
    }
}

int recupere(const Liste *liste, int n)
{
    Noeud *iterateur = liste->premier;
    for (int i = 1; i < n + 1; i++)
    {
        iterateur = iterateur->suivant;
    }
    return iterateur->donnee;
}

int cherche(const Liste *liste, int valeur)
{
    int index = 1;
    Noeud *iterateur = liste->premier;
    while (iterateur != nullptr)
    {
        if (iterateur->donnee == valeur)
        {
            return index;
        }
        iterateur = iterateur->suivant;
    }
    return -1;
}

void stocke(Liste *liste, int n, int valeur)
{
    Noeud *iterateur = liste->premier;
    for (int i = 1; i < n + 1; i++)
    {
        iterateur = iterateur->suivant;
    }
    iterateur->donnee = valeur;
}

void ajoute(DynaTableau *tableau, int valeur)
{
    if (tableau->taille > tableau->capacite){
        tableau->capacite++;
        int *nvtab = new int[tableau->capacite];
        for (int i = 0; i < tableau->capacite;){
            nvtab[i] = tableau->tab[i];
        }
        nvtab[tableau->capacite] = valeur;
        tableau->tab = nvtab;
        tableau->taille++;
    }
    else{
        tableau->tab[tableau->taille] = valeur;
        tableau->taille++;
    }
}

void initialise(DynaTableau *tableau, int capacite)
{
    tableau->tab = new int[capacite];
    tableau->capacite = capacite;
}

bool est_vide(const DynaTableau *liste)
{
    if(liste->taille == 0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau *tableau)
{
        for(int i=0; i<tableau->taille; i++){
            cout << tableau->tab[i] << endl;
        }
}

int recupere(const DynaTableau *tableau, int n)
{
    if(n > tableau->taille){
        return -1;
    }
    return tableau->tab[n-1];
}

int cherche(const DynaTableau *tableau, int valeur)
{
    for(int i = 0; i< tableau->taille; i++){
        if(tableau->tab[i] == valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau *tableau, int n, int valeur)
{
    if(n > tableau->capacite){
        tableau->capacite = n;
    }
    if(n > tableau->taille){
        tableau->taille = n;
    }
    tableau->tab[n-1] = valeur;
}

// void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste *liste, int valeur)
{
    Noeud* iterateur = liste->premier;
    while(iterateur->suivant!=nullptr){
        iterateur = iterateur->suivant;
    }
    Noeud* nv = new Noeud;
    nv->donnee = valeur;
    iterateur->suivant = nv;
}

// int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud* premier = liste->premier;
    liste->premier = liste->premier->suivant;
    return premier->donnee;
}

// void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste *liste, int valeur)
{
    Noeud* nv = new Noeud;
    nv->donnee = valeur;
    nv->suivant = liste->premier;
    liste->premier = nv;
}

// int retire_pile(DynaTableau* liste)
int retire_pile(Liste *liste)
{
    Noeud* premier = liste->premier;
    liste->premier = liste->premier->suivant;
    return premier->donnee;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i = 1; i <= 7; i++)
    {
        ajoute(&liste, i * 7);
        ajoute(&tableau, i * 5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i = 1; i <= 7; i++)
    {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while (!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while (!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
